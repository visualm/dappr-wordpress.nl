<?php
        /* Template Name: 404 */

    get_header();

    get_template_part('template-parts/section', 'hero');
    get_template_part('template-parts/section', 'breadcrumb');
    get_template_part('template-parts/section', '404');
    get_template_part('template-parts/section', 'news');
    get_template_part('template-parts/section', 'cta');

    get_footer();
 ?>
