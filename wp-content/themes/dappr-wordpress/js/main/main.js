
$( document ).ready(function() {
    $(".hamburger").click(function() {
        $(this).toggleClass("cross");
        $(".main-navigation").toggleClass("open");
        $("body").toggleClass("noscroll");
    });

    $('.arrow i').click(function() {
        $(this).toggleClass('down');
    });

    $('.arrow').click(function() {
        $(this).next().toggleClass('open');
        $(this).toggleClass('active');
    });

    if ($('ul.submenu li').hasClass('active')) {
        $('ul.submenu li').parent().addClass('open');
        $('.arrow').addClass('active');
        $('.arrow i').addClass('down');
    }

    var header = $(".main-navigation");

    if ($(window).width() >= 992) {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 120) {
                header.removeClass('normal').addClass("fixed");
            } else {
                header.removeClass("fixed").addClass('normal');
            }
        });
    }
});

$(document).on('focus', '.gfield input[type=text], .gfield input[type=email], .gfield textarea', function() {
    $(this).closest('.gfield').find('.gfield_label').addClass('focused');
}).on('blur', '.gfield input[type=text], .gfield input[type=email], .gfield textarea', function() {
    if ($(this).val().trim() === "") {
        $(this).closest('.gfield').find('.gfield_label').removeClass('focused');
    }
});

if ($('.swiper-container .swiper-slide').length > 1) {
    //Horizontal Swiper
    var swiper = new Swiper('.horizontal', {
        centeredSlides: true,
        loop: true,
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
    });

    // Vertical Swiper
    var swiper = new Swiper('.vertical', {
        direction: 'vertical',
        centeredSlides: true,
        loop: true,
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
    });

    // multiple Swiper
    var swiper = new Swiper('.multiple', {
        slidesPerView: 1,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
         breakpoints: {
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            992: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
          }
    });
}
