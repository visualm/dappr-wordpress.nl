<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Site title -->
    <title><?php wp_title(''); ?></title>

    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Stylesheets -->
    <style>
    <?= include 'dist/css/style.critical.css';?>
    </style>
    <link rel="stylesheet" href="https://kit.fontawesome.com/5f519bc7e7.css" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=autoVer('/dist/css/style.critical.css');?>">
    <link rel="stylesheet" href="<?=autoVer('/dist/css/style.css');?>">

    <!-- Include WP Head -->
    <?php wp_head(); ?>

    <!-- Include cookies -->
    <script id="cookieconsent" defer src="<?php echo get_template_directory_uri().'/cookie/cookieconsent.js'; ?>"></script>
    <!--   -->
</head>

<!-- Begin body -->
<body <?php body_class(); ?>>
    <?php get_template_part('template-parts/section', 'menu');
