//DAPPR COOKIECONSENT LOADER
//This script can be copied to and installed in any website. It wil add files from DAPPR site to header of any site and start script with the settings that are defined in this file.

window.cc_data_custom = {

    //Logo to show in settings popup
    logo: 'https://www.dappr.nl/cookie/logo.svg',
    privacypage: 'https://www.dappr.nl/privacybeleid/',
	contactpage: 'https://www.dappr.nl/contact/',
    
    //EXAMPLE SETTINGS (copy of data object in original cookieconsent_init.js)
    // current_lang : 'nl',   
    // gui_options: {
    //     consent_modal: {
    //         layout: 'box',                      // box,cloud,bar
    //         position: 'bottom left',            // bottom,middle,top + left,right,center
    //         transition: 'zoom'                  // zoom,slide
    //     },
    //     settings_modal: {
    //         layout: 'box',                      // box,bar
    //         transition: 'zoom'                  // zoom,slide
    //     }
    // },    
    // languages: {
    //     'nl': {
    //         custom_items:{
    //             third_party_text: "",
    //             third_party_label: "Accepteren.",
    //         },
    //         consent_modal: {
    //             title: `Wij gebruiken cookies!`,                
    //         },
    //         settings_modal: {
    //             title: `testen`,               
    //         }
    //     },
    // }  
          
};

//CSS VARIABLES USED (will override default settings in cookieconcent.css)
window.cc_styles = {
    //DEFAULT OVERRIDE(S)
    'cc-btn-primary-bg': "#25d366",   

    //OTHER VARIABLES
    // 'cc-bg': "#fff",
    // 'cc-text': "#2d4156",
    // 'cc-border-radius': ".35em",     
    // 'cc-btn-primary-hover-bg': "#1d2e38",
    // 'cc-btn-secondary-bg': "#eaeff2",    
    // 'cc-btn-secondary-hover-bg': "#d8e0e6",
    // 'cc-btn-border-radius': "4px",
    // 'cc-toggle-bg-off': "#919ea6",    
    // 'cc-toggle-bg-readonly': "#d5dee2",
    // 'cc-toggle-knob-bg': "#fff",
    // 'cc-toggle-knob-icon-color': "#ecf2fa",    
    // 'cc-cookie-category-block-bg': "#f0f4f7",
    // 'cc-cookie-category-block-bg-hover': "#e9eff4",
    // 'cc-section-border': "#f1f3f5",
    // 'cc-cookie-table-border': "#e9edf2",
    // 'cc-overlay-bg': "rgba(4, 6, 8, .85)",
    // 'cc-webkit-scrollbar-bg': "#cfd5db",
    // 'cc-webkit-scrollbar-bg-hover': "#9199a0",

    //VARIABLES WITH DEFAULT VALUE FORWARDED TO OTHER VARIABLES
    //'cc-btn-primary-text': "var(--cc-bg)",
    //'cc-btn-secondary-text': var(--cc-text);
    //'cc-toggle-bg-on': var(--cc-btn-primary-bg);
    //'cc-block-text': var(--cc-text);
}

//function will be executed BEFORE cookieconsent_execute() in cookieconsent_init.js
function cookieconsent_custom_pre_execute(cc){    
}

//function will be executed AFTER cookieconsent_execute() in cookieconsent_init.js
function cookieconsent_custom_post_execute(cc){  
}



//LOAD INITIALIZATION SCRIPT FROM CENTRAL SITE
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = "https://cookieconsent.dapprgemaakt.nl/cookieconsent_init.js";
document.head.appendChild(script);