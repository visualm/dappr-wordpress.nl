<footer class="footer">

    <div class="container">
        <div class="row">


            <div class="col-12 col-lg-4">
                <h4><?php _e('Adresgegevens', 'Footer'); ?></h4>
                <?php if (get_field( 'name', 'option' )): ?>
                    <span><strong><?php the_field( 'name', 'option' ); ?></strong></span>
                <?php endif; ?>

                <?php if (get_field( 'adres', 'option' )): ?>
                    <span><?php the_field( 'adres', 'option' ); ?></span>
                <?php endif; ?>

                <?php if (get_field( 'zipcode', 'option' ) || get_field( 'city', 'option')): ?>
                    <span><?php the_field( 'zipcode', 'option' ); ?> <?php the_field( 'city', 'option' ); ?></span>
                <?php endif; ?>

                <?php if (get_field( 'phone', 'option' )): ?>
                    <span><a href="<?=get_field('phone_link', 'options');?>" aria-label="<?=get_field('phone', 'options');?>"><i class="fas fa-phone"></i><?=get_field('phone', 'options');?></a></span>
                <?php endif; ?>

                <?php if (get_field( 'phone', 'option' )): ?>
                    <span><a href="<?=get_field('phone_link', 'options');?>" aria-label="<?=get_field('phone', 'options');?>"><i class="fas fa-phone"></i><?=get_field('phone', 'options');?></a></span>
                <?php endif; ?>

                <?php if (get_field( 'email', 'option' )): ?>
                    <span><a href="<?=antispambot(get_field( 'email_link', 'option' ));?>" aria-label="mailto:<?=antispambot(get_field( 'email', 'option' ));?>"><i class="fas fa-envelope"></i><?=antispambot(get_field( 'email', 'option' ));?></a></span>
                <?php endif; ?>
            </div>
            <div class="col-12 col-lg-4">
                <h4><?php _e('Volg ons op socialmedia', 'Footer'); ?></h4>
                <span class="socials">
                    <?php if (get_field('linkedin', 'option')): ?>
                        <a href="<?php the_field( 'linkedin', 'option' ); ?>" target="_blank" aria-label="<?php the_field( 'linkedin', 'option' ); ?>"><i class="fab fa-linkedin"></i></a>
                    <?php endif; ?>

                    <?php if (get_field('x', 'option')): ?>
                        <a href="<?php the_field( 'x', 'option' ); ?>" target="_blank" aria-label="<?php the_field( 'x', 'option' ); ?>"><i class="fa-brands fa-square-x-twitter"></i></a>
                    <?php endif; ?>

                    <?php if (get_field('facebook', 'option')): ?>
                        <a href="<?php the_field( 'facebook', 'option' ); ?>" target="_blank" aria-label="<?php the_field( 'facebook', 'option' ); ?>"><i class="fab fa-facebook-square"></i></a>
                    <?php endif; ?>

                    <?php if (get_field('instagram', 'option')): ?>
                        <a href="<?php the_field( 'instagram', 'option' ); ?>" target="_blank" aria-label="<?php the_field( 'instagram', 'option' ); ?>"><i class="fab fa-instagram"></i></a>
                    <?php endif; ?>

                    <?php if (get_field('youtube', 'option')): ?>
                        <a href="<?php the_field( 'youtube', 'option' ); ?>" target="_blank" aria-label="<?php the_field( 'youtube', 'option' ); ?>"><i class="fab fa-youtube-square"></i></a>
                    <?php endif; ?>
                </span>
            </div>

            <div class="col-12 col-lg-4">
                <h4>Links</h4>
                <nav class="nav-footer">
                    <?php wp_nav_menu( array(
                        'theme_location' => 'footer-menu',
                        'depth' => '1',
                    )); ?>
                </nav>
            </div>

        </div>
    </div>

    <div class="container">
        <div class="dappr">
            <a href="https://www.dappr.nl/" target="_blank" aria-label="dappr gemaakt">
                <img class="dappr-gemaakt" src="<?php echo get_template_directory_uri() . '/img/dappr.svg'; ?>" alt="dappr.gemaakt">
            </a>
        </div>
    </div>
</footer>
<script>
    function extractYouTubeID(url) {
        var regExp = /^.*(youtu\.be\/|youtube\.com\/embed\/)([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match && match[2].length === 11) ? match[2] : null;
        }
        function playVideo(element) {
        var container = element.closest('.wp-block-embed-youtube, .wp-block-embed');
        var iframe = container.querySelector('iframe');
        var youtubeId = extractYouTubeID(iframe.src);
        container.innerHTML = '<iframe class="youtube-video" src="https://www.youtube.com/embed/' + youtubeId + '?autoplay=1&enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allowfullscreen allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>';
        }
        document.addEventListener("DOMContentLoaded", function() {
        var gutenbergYoutubeBlocks = document.querySelectorAll('.wp-block-embed-youtube, .wp-block-embed');
        gutenbergYoutubeBlocks.forEach(function(block) {
            var iframeWrapper = block.querySelector('.wp-block-embed__wrapper'); // Target the wrapper of the iframe
            if (!iframeWrapper) return; // If no wrapper found, skip to next block
            var iframe = iframeWrapper.querySelector('iframe');
            var youtubeId = extractYouTubeID(iframe.src);
            // Create an overlay div and append it to the iframe wrapper
            var overlay = document.createElement('div');
            overlay.className = 'overlay';
            overlay.onclick = function() { playVideo(this); };
            overlay.style.backgroundImage = 'url(https://img.youtube.com/vi/' + youtubeId + '/maxresdefault.jpg)';
            overlay.innerHTML = '<i class="fa-regular fa-circle-play play-button"></i>';
            iframeWrapper.appendChild(overlay); // Append the overlay to the iframe's wrapper
        });
    });
</script>

<script type="text/javascript" defer>
[
    '<?=autoVer('/dist/js/jquery/jquery-3.5.1.min.js');?>',
    '<?=autoVer('/dist/js/bootstrap/bootstrap.min.js');?>',
    '<?=autoVer('/dist/js/bootstrap/bootstrap.bundle.min.js');?>',
    '<?=autoVer('/dist/js/swiper/swiper-bundle.min.js');?>',
    '<?=autoVer('/dist/js/main.min.js');?>',
].forEach(function(src) {
    var script = document.createElement('script');
    script.src = src;
    script.async = false;
    document.body.appendChild(script);
});
</script>

<?php wp_footer(); ?>
</body>
</html>
