
<?php

    //Get the header
    get_header();

    get_template_part('template-parts/section', 'hero');
    get_template_part('template-parts/section', 'content');

    //Check if news items should be shown.
    if (get_field('news_on') === true) {
        get_template_part('template-parts/section', 'news');
    }

    //Check if cta should be shown.
    if (get_field('cta_on') === true) {
        get_template_part('template-parts/section', 'cta');
    }

    get_footer();

    ?>