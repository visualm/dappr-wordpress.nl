<?php
    require_once '../../../../wp-load.php';
	require("sendgrid/sendgrid-php.php");

	if(!$_POST) exit;

	if($_POST)
	{

        // custom (acf) contact form fields
        $contactEmailFrom 		= get_field('cont_email_from', 'options' ); // email from
        $contactEmailTo 		= get_field( 'cont_email_to', 'options' ); // email to
        $contactEmailSubject 	= get_field( 'cont_email_subject', 'options' ); // subject
        $contactEmailRedirect 	= get_field( 'cont_email_redirect', 'options' ); // redirect

		$sendgrid = new SendGrid(get_field( 'sendgrid_api', 'options' ));
		//check if its an ajax request, exit if not
		if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
			$output = json_encode(array( //create JSON data
				'type'=>'error',
				'text' => 'Sorry Request must be Ajax POST'
			));
			die($output); //exit script outputting json data
		}

		//Sanitize input data using PHP filter_var().

		$name    				= filter_var($_POST["name"], FILTER_SANITIZE_STRING);
		$email   				= filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
		$company    			= filter_var($_POST["company"], FILTER_SANITIZE_STRING);
		$role				    = filter_var($_POST["role"], FILTER_SANITIZE_STRING);
		$message				= filter_var($_POST["message"], FILTER_SANITIZE_STRING);

		// default from
		$from = "no-reply@dappr.nl";
		if (!empty($contactEmailFrom)) {
			$from = $contactEmailFrom;
		}

        // default subject
        $subject = 'Nieuwe inzending vanaf blog' . get_bloginfo('name');
        if (!empty($contactEmailSubject)) {
            $subject = $contactEmailSubject;
        }

        if (!empty($contactEmailTo)){
            $to = $contactEmailTo;
        } elseif($email = get_field('email' , 'options')) {
			// default mail to option (Theme setting|contact|e-mailadres)
			$to = $email;
		} else {
			$output = json_encode(array( //create JSON data
				'type'=>'error',
				'text' => 'Sorry mailTo is not set!'
			));
			die($output); //exit script outputting json data
		}




$message_body=<<<EOMSG
<strong>Naam:</strong> <br />
$name
<br />
<br />
<strong>E-mail:</strong><br />
$email
<br />
<br />
<strong>Bedrijf:</strong> <br />
$company
<br />
<br />
<strong>Functie:</strong><br />
$role
<br />
<br />
<strong>Bericht:</strong> <br />
$message
EOMSG;


		$sndemail 	= new SendGrid\Email();
		$sndemail
		->addTo($to)
		->setFrom($from)
		->setReplyto($email)
		->setSubject($subject)
		->setHtml($message_body)
		->setHeaders(array())
		;
		$res = $sendgrid->send($sndemail);


		// var_dump($sndemail);
		// die();
		$res = $sendgrid->send($sndemail);
		if($res) {
            $response = array('type'=>'message', 'text' => '<strong>Bedankt voor het invullen!</strong> Uw bericht is succesvol verzonden. Wij nemen zo spoedig mogelijk contact met u op.');
            if (!empty($contactEmailRedirect)) {
                $response['redirect'] = true;
                $response['type'] = 'redirect';
                $response['redirectURL'] = $contactEmailRedirect;
            }

            die(json_encode($response));
		}
	}
?>
