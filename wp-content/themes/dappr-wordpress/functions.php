<?php
    require_once (TEMPLATEPATH . '/functions/init.php'); // initiates theme settings
    require_once (TEMPLATEPATH . '/functions/helpers.php'); // add custom helpers
    require_once (TEMPLATEPATH . '/functions/theme-settings.php'); // theme settings for WP.
    require_once (TEMPLATEPATH . '/functions/theme-support.php'); // wordpress/theme settings
    require_once (TEMPLATEPATH . '/functions/admin.php'); // theme options /admin
    require_once (TEMPLATEPATH . '/functions/menu.php'); // dappr login /manage
    require_once (TEMPLATEPATH . '/functions/custom-functions.php'); // custom functions
 ?>
