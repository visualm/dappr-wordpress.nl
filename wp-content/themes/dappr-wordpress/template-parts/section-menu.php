<header class="menu">
    <div class="container-fluid" style="padding: 0;">
        <div class="row menu-inner" style="margin: 0;">
            <div class="col" style="padding: 0;">
                <?php if (get_field('logo_dark', 'options')): ?>
                    <div class="mobile-logo">
                        <a href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" aria-label="Logo">
                            <img src="<?php the_field('logo_dark', 'options'); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" class="logo-img"/>
                        </a>
                    </div>
                <?php endif; ?>
                <div class="main-navigation normal">
                    <?php if (get_field('logo_dark', 'options')): ?>
                        <div class="logo normal">
                            <a href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" aria-label="Navigatie">
                                <img src="<?php the_field('logo_dark', 'options'); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" class="logo-img"/>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if (get_field('logo_light', 'options')): ?>
                        <div class="logo fixed">
                            <a href="<?php echo get_site_url(); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" aria-label="Logo">
                                <img src="<?php the_field('logo_light', 'options'); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>" class="logo-img"/>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="navigation">
                        <ul class="nav-ul">
                            <?php echo generateMenuItems('primary'); ?>
                        </ul>
                    </div>
                </div>
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
     </div>
</header>
