<?php
	$args = array(
		'post_type' 		=> 'post',
		'posts_per_page' 	=> '9',
		'post_status'		=> 'publish',
		'orderby'           => 'publish_date',
        'order'             => 'desc',
	);

	$news = new WP_Query( $args );
?>

<?php if ( $news->have_posts() ) : ?>
    <section class="s-news full">
        <div class="container">
            <div class="row">
                <?php while ( $news->have_posts() ) : $news->the_post(); ?>
                    <div class="col-12 col-lg-4">
                        <div class="card">
                        <?php if (get_the_post_thumbnail_url()): ?>
                              <img class="card-img-top img-fluid" src="<?=get_the_post_thumbnail_url($post->ID ,'news');?>" alt="Foto - <?=the_title();?>">
                          <?php else : ?>
                              <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png" alt="Foto - <?=the_title();?>">
                          <?php endif; ?>
                          <div class="card-body">
                            <h5 class="card-title"><?=the_title();?></h5>
                            <p class="card-text"><?=the_excerpt();?></p>
                            <a href="<?=the_permalink();?>" class="btn btn-primary" aria-label="Lees meer..">Lees meer..</a>
                          </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
	<?php wp_reset_postdata(); ?>

<?php endif; ?>
