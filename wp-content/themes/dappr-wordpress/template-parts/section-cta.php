
<?php if (get_field( 'cta_bg', 'option' ) || get_field( 'cta_title', 'option' )): ?>
    <section class="s-cta">

        <?php if (get_field('cta_bg', 'option')): ?>
            <div class="background" style="background-image: url('<?php the_field('cta_bg', 'option'); ?>');">
            <div class="cta-overlay"></div>
            <div class="container">
                <div class="cta-content">
                    <div class="text-center">
                        <?php if (get_field( 'cta_title', 'option' )): ?>
                            <h2><?php the_field( 'cta_title', 'option' ); ?></h2>
                        <?php endif; ?>

                        <?php if (get_field( 'cta_content', 'option' )): ?>
                            <p><?php the_field( 'cta_content', 'option' ); ?></p>
                        <?php endif; ?>

                        <?php if (get_field('cta_btn_url', 'option')): ?>
                            <div class="buttons">
                                <a href="<?php the_field( 'cta_btn_url', 'option' ); ?>" class="btn btn-secondary" aria-label="<?php the_field( 'cta_btn_text', 'option' ); ?>" ><?php the_field( 'cta_btn_text', 'option' ); ?><i class="fas fa-arrow-right"></i></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div><!-- cta-content -->
            </div>

            </div> <!-- background -->
        <?php else : ?>
            <img class="background" src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png">
        <?php endif; ?>

    </section>
<?php endif; ?>
