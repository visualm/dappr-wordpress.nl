<section class="location">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">

                <div class="location-route">
                    <h2><?php _e('Bezoek ons!', 'dappr'); ?></h2>

                    <input id="startInput" class="form-control" type="text" placeholder="<?php _e('Uw vertrekpunt', 'Jobo'); ?>"> <br>
                    
                    <a id="navigateLink" class="btn btn-primary" href="#" target="_blank" aria-label="Navigate">
                        <?php _e('Navigeren', 'Jobo'); ?>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="s-maps">
                    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
                    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
                    <style>
                        .marker {
                            background-image: url('<?=get_template_directory_uri();?>/img/marker.svg');
                            background-size: cover;
                            width: 50px;
                            height: 50px;
                            border-radius: 50%;
                            cursor: pointer;
                        }
                        #map {
                            width: 100%;
                            height: 500px;
                        }
                    </style>
                    <div id='map' data-lat="<?php the_field('latitude', 'option'); ?>" data-long="<?php the_field('longitude', 'option'); ?>" data-map-key="<?php the_field('map_key', 'option'); ?>" data-route="<?php the_field('route', 'option'); ?>"></div>
                        <script>
                            var mapElement = document.getElementById('map');

                            if (mapElement) {
                                var latitude = mapElement.getAttribute('data-lat');
                                var longitude = mapElement.getAttribute('data-long');
                                var mapKey = mapElement.getAttribute('data-map-key');
                                var route = mapElement.getAttribute('data-route');
                                
                            }

                            mapboxgl.accessToken = mapKey;
                                    
                            var map = new mapboxgl.Map({
                                container: 'map', // container id
                                // style: 'mapbox://styles/mapbox/light-v10', // stylesheet location
                                style: 'mapbox://styles/mapbox/streets-v10', // stylesheet location
                                center: [longitude, latitude], 
                                zoom: 16 // starting zoom
                            });

                            var geojson = {
                                type: 'FeatureCollection',
                                features: [{
                                    type: 'Feature',
                                    geometry: {
                                        type: 'Point',
                                        coordinates: [longitude, latitude], // starting position [lng, lat]
                                    },
                                    properties: {
                                        title: 'Dappr',
                                        description: 'Merk, Marketing & Online'
                                    }
                                }]
                            };
                            // add markers to map
                            geojson.features.forEach(function(marker) {

                            // create a HTML element for each feature
                            var el = document.createElement('div');
                            el.className = 'marker';

                            // make a marker for each feature and add to the map
                            new mapboxgl.Marker(el)
                                .setLngLat(marker.geometry.coordinates)
                                .addTo(map);
                            });


                            // Routebeschrijving. 
                            document.addEventListener("DOMContentLoaded", function() {
                                var t = document.getElementById("startInput");
                                var link = document.getElementById("navigateLink");

                                // If both elements exist
                                if(t && link) {
                                    link.addEventListener("click", function(e) {
                                        e.preventDefault();
                                        var location = t.value;
                                        // Plak hier de route URL, bijvoorbeeld: /Dappr,+Van+Limburg+Stirumstraat,+Hoogeveen
                                        location = "https://www.google.com/maps/dir/" + encodeURIComponent(location) + route;
                                        // console.log(location);
                                        window.open(location, "_blank")
                                    });
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
