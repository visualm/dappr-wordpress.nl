<?php
	$args = array(
		'post_type' 		=> 'post',
		'posts_per_page' 	=> '3',
		'post_status'		=> 'publish',
		'orderby'           => 'publish_date',
        'order'             => 'desc',
	);

	$news = new WP_Query( $args );
?>

<?php if ( $news->have_posts() ) : ?>
    <section class="s-news">
        <div class="container">
            <h2><?_e('Laatste nieuws', 'Nieuws')?></h2>
            <div class="row">
                <?php while ( $news->have_posts() ) : $news->the_post(); ?>
                    <div class="col-12 col-lg-4">

                        <div class="card">
                            <a href="<?=the_permalink();?>" aria-label="Lees meer..">
                                <?php if (get_the_post_thumbnail_url()): ?>
                                  <span class="card-img-top" style="background-image: url('<?=get_the_post_thumbnail_url($post->ID ,'news');?>');" title="Foto - <?=the_title();?>">
                                <?php else : ?>
                                  <span class="card-img-top" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/placeholder.png');" title="Foto - <?=the_title();?>">
                                <?php endif; ?>
                            </a>
                          <div class="card-body">
                            <span class="card-title"><?=the_title();?></span>
                            <p class="card-text"><?=the_excerpt();?></p>
                            <a href="<?=the_permalink();?>" aria-label="Lees meer.." class="btn btn-primary">Lees meer..</a>
                          </div>
                        </div>

                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
	<?php wp_reset_postdata(); ?>

<?php endif; ?>
