<span id="formscroll"></span>
<div class="row">

    <div class="col-12 col-lg-4">
        <div class="contact-address">
            <h3><?=__('Adresgegevens', 'Contact');?></h3>
            <?php if (get_field( 'name', 'option' )): ?>
                <span><strong><?php the_field( 'name', 'option' ); ?></strong></span>
            <?php endif; ?>

            <?php if (get_field( 'adres', 'option' )): ?>
                <span><?php the_field( 'adres', 'option' ); ?></span>
            <?php endif; ?>

            <?php if (get_field( 'zipcode', 'option' ) || get_field( 'city', 'option')): ?>
                <span><?php the_field( 'zipcode', 'option' ); ?><?php the_field( 'city', 'option' ); ?></span>
            <?php endif; ?>

            <?php if (get_field( 'phone', 'option' )): ?>
                <span><a href="tel:<?=get_field('phone', 'options');?>"><i class="fas fa-phone"></i><?=get_field('phone', 'options');?></a></span>
            <?php endif; ?>

            <?php if (get_field( 'email', 'option' )): ?>
                <span><a href="mailto<?=antispambot(get_field( 'email', 'option' ));?>"><i class="fas fa-envelope"></i><?=antispambot(get_field( 'email', 'option' ));?></a></span>
            <?php endif; ?>

            <?php if (get_field( 'email_support', 'option' )): ?>
                <span><a href="mailto<?=antispambot(get_field( 'email_support', 'option' ));?>"><i class="fas fa-headset"></i><?=antispambot(get_field( 'email_support', 'option' ));?></a></span>
            <?php endif; ?>
            <hr>
            <?php if (get_field( 'kvk', 'option' )): ?>
                <span><strong>KvK: </strong><?php the_field( 'kvk', 'option' ); ?></span>
            <?php endif; ?>

            <?php if (get_field( 'btw', 'option' )): ?>
                <span><strong>BTW: </strong><?php the_field( 'btw', 'option' ); ?></span>
            <?php endif; ?>

            <?php if (get_field( 'iban', 'option' )): ?>
                <span><strong>iBAN: </strong><?php the_field( 'iban', 'option' ); ?></span>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-12 col-lg-8">
        <form class="dynamic-ajax-form contact" action="<?=get_template_directory_uri();?>/mail/mail-contact.php" method="post">
            <div class="row">
                <div class="col-6">
                    <input type="text" class="form-control required" name="name" required value="" placeholder="<?php _e('Contactpersoon', 'Dappr'); ?>*..." />
                </div>
                <div class="col-6">
                    <input type="text" class="form-control required" name="company" required value="" placeholder="<?php _e('Bedrijfsnaam', 'Dappr'); ?>*..." />
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <input type="email" class="form-control required" name="email" required value="" placeholder="<?php _e('E-mailadres', 'Dappr'); ?>*..." />
                </div>
                <div class="col-6">
                    <input type="text" class="form-control required" name="phone" required value="" placeholder="<?php _e('Telefoon', 'Dappr'); ?>*..." />
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <textarea class="form-control required" name="message" rows="8" cols="80" placeholder="<?php _e('Typ hier je bericht', 'Dappr');?>*..."></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button id="send" name="button" class="button btn btn-primary"><?php _e('Bericht verzenden', 'Dappr'); ?></button>
                </div>
            </div>
        </form>
    </div>

</div>
