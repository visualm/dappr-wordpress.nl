
<?php if ( get_field( 'hero_page_image' ) ) : ?>


    <section class="s-hero" style="background-image: url(<?php the_field( 'hero_page_image' ); ?>); height: 410px; text-align: center;">
    <div class="swiper-overlay"></div>
        <div class="container">
            <div class="page-title">
                <span class="hero-title">
                  <?php the_field( 'hero_page_title' ); ?>
                </span>
            </div>
        </div>
    </section>

<?php else : ?>

        <section class="s-hero fallback" style="background-image: url('<?php the_field( 'hero_fallback', 'options' ); ?>'); height: 410px; text-align: center;">
        <div class="swiper-overlay"></div>
            <div class="container">
                <div class="page-title">
                    <span class="hero-title">
                        <?php echo get_the_title(); ?>
                    </span>
                </div>
            </div>
        </section>


<?php endif; ?>
