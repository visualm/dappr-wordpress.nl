<?php
    if (get_the_post_thumbnail_url()) {
        $background = get_the_post_thumbnail_url();
    } else {
        $background = get_field('hero_fallback', 'options');
    }
 ?>

<section class="s-hero-big">
   <!-- Swiper -->
    <div class="swiper-container <?php the_field('hero_slider_type'); ?>">
        <div class="swiper-wrapper">
            <?php if ( have_rows( 'slider' ) ) : ?>
                <?php while ( have_rows( 'slider' ) ) : the_row(); ?>
                    <div class="swiper-slide" style="background-image: url(<?php the_sub_field('hero_slider_img'); ?>)">
                        <div class="swiper-overlay"></div>
                        <div class="container">
                            <div class="inner">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <?php if (get_sub_field('hero_slider_title')) : ?>
                                            <span class="slider-title">
                                                <?php the_sub_field('hero_slider_title'); ?>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('hero_slider_content')) : ?>
                                            <span class="slider-content">
                                                <?php the_sub_field('hero_slider_content'); ?>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('hero_slider_btn_url')) : ?>
                                            <span class="slider-button">
                                                <a class="btn btn-primary" href="<?php the_sub_field('hero_slider_btn_url'); ?>" aria-label="<?php the_sub_field('hero_slider_btn_text'); ?>">
                                                    <?php the_sub_field('hero_slider_btn_text'); ?><i class="fas fa-arrow-right"></i>
                                                </a>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php elseif( have_rows( 'hero' ) ) : ?>
                <?php while ( have_rows( 'hero' ) ) : the_row(); ?>
                    <div class="hero-fallback" style="background-image: url(<?php the_sub_field('hero_image'); ?>)">
                        <div class="swiper-overlay"></div>
                        <div class="container">
                            <div class="inner">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <?php if (get_sub_field('hero_title')) : ?>
                                            <span class="slider-title">
                                                <?php the_sub_field('hero_title'); ?>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('content')) : ?>
                                            <span class="slider-content">
                                                <?php the_sub_field('content'); ?>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (get_sub_field('hero_button_url')) : ?>
                                            <span class="slider-button">
                                                <a class="btn btn-primary" href="<?php the_sub_field('hero_button_url'); ?>">
                                                    <?php the_sub_field('hero_button_text'); ?><i class="fas fa-arrow-right"></i>
                                                </a>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <div class="hero-fallback" style="background-image: url('<?php echo $background;?>');">
                    <div class="swiper-overlay"></div>
                    <div class="container">
                        <div class="inner">
                            <div class="row">
                                <div class="col-12 col-lg-4">
                                    <?php if (get_field('hero_title')) : ?>
                                        <span class="slider-title">
                                            <?php the_field( 'hero_title' ); ?>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

</section>
