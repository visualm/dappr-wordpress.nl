
<?php
    /* Template Name: Nieuwsoverzicht */

    get_header();

    get_template_part('template-parts/section', 'hero');
    get_template_part('template-parts/section', 'breadcrumb');
    get_template_part('template-parts/section', 'content');
    get_template_part('template-parts/section', 'newsfull');

    //Check if cta should be shown.
    if (get_field('cta_on') === true) {
        get_template_part('template-parts/section', 'cta');
    }

    get_footer();
?>
