<?php
function generateSubMenuItems($raw_menu, $menu_item) {
    global $post;
    

    if (!empty($raw_menu)) {
        $submenu = array();
        $submenu['submenu_items'] = '';
        $submenu['active_subitem'] = '';
        
        foreach ($raw_menu as $key => $submenu_item) {
            if ($submenu_item->menu_item_parent == $menu_item->ID) { // Checking if the menu item is the parent ID of the child

                $childPageID = url_to_postid($submenu_item->url); // Check if item is an active page view.
                if ($childPageID == $post->ID) {
                    $submenu['active_subitem'] = true;
                    $active = 'active';
                } else {
                    $active = '';
                }
                
                $submenu['submenu_items'] .= '<li id="sub-item-' . $submenu_item->ID . '" class="' . $active . '">';
                $submenu['submenu_items'] .= '<a href="' . $submenu_item->url . '" title="' . $submenu_item->title . '">' . $submenu_item->title . '</a>';
                $submenu['submenu_items'] .= '</li>';
            }
        }
    }
    return $submenu;
}
function generateMenuItems($menu_name) {
    global $post;
    $raw_menu = wp_get_nav_menu_items($menu_name); // Getting the raw menu data
    $output = ''; // Declaring the output variable

    foreach ($raw_menu as $menu_item) {
        if (empty($menu_item->menu_item_parent)) { // Checking to see if menu item has parent or not.
            $parentPageID = url_to_postid($menu_item->url);
            $submenu = generateSubMenuItems($raw_menu, $menu_item); // Function returns data when menu item has children

            if ($parentPageID == $post->ID ) {
                $active = 'active';
            } else if (!empty($submenu['active_subitem']) && $submenu['active_subitem'] == true) {
                $active = 'active';
            } else {
                $active = '';
            }

            $output .= '<li id="menu-item-' . $menu_item->ID . '" class="' . $active . '">';
            $output .= '<a href="' . $menu_item->url . '" title="' . $menu_item->title . '">' . $menu_item->title . '</a>';
            
            
            if (!empty($submenu['submenu_items'])) { // If not empty, place sub menu items
                $output .= '<span class="arrow"><i class="fas fa-chevron-right"></i></span>'; // Adding an icon when submenu exists
                $output .= '<ul class="submenu">';
                $output .= $submenu['submenu_items']; // Adding the submenu to the output
                $output .= '</ul>';
            }
            $output .= '</li>';
        }
    }
    return $output;
}