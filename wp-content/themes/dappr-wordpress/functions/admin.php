<?php

//******************************************************************************
// Remove userroles
remove_role( 'subscriber' );
remove_role( 'contributor' );
remove_role( 'author' );

if (get_role('wpseo_manager')) {
	remove_role( 'wpseo_manager' );
}

if (get_role('wpseo_editor')) {
	remove_role( 'wpseo_editor' );
}


//******************************************************************************
// Rename editor to Klant
function change_role_name() {
    global $wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    //You can list all currently available roles like this...
    // $roles = $wp_roles->get_names();
    // print_r($roles);

    //You can replace "administrator" with any other role "editor", "author", "contributor" or "subscriber"...
    $wp_roles->roles['administrator']['name'] = 'dappr';
    $wp_roles->role_names['administrator'] = 'dappr';

	$wp_roles->roles['editor']['name'] = 'Klant';
	$wp_roles->role_names['editor'] = 'Klant';
}
add_action('init', 'change_role_name');


//******************************************************************************
// add caps for editor ( klant )
$klant = get_role('editor');
$klant->add_cap('edit_theme_options');
$klant->remove_cap('customize');

$klant->add_cap('edit_users');
$klant->add_cap('list_users');
$klant->add_cap('create_users');
$klant->add_cap('add_users');
$klant->add_cap('delete_users');

//******************************************************************************
// remove caps for editor ( klant )
$klant = get_role('editor');
$klant->remove_cap('customize');


//******************************************************************************
// Hide admin from user list for editor ( klant )
function vm_pre_user_query($user_search) {
    $user = wp_get_current_user();

    if ( ! current_user_can( 'manage_options' ) ) {
        global $wpdb;

        $user_search->query_where =
            str_replace('WHERE 1=1',
            "WHERE 1=1 AND {$wpdb->users}.ID IN (
                 SELECT {$wpdb->usermeta}.user_id FROM $wpdb->usermeta
                    WHERE {$wpdb->usermeta}.meta_key = '{$wpdb->prefix}capabilities'
                    AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%')",
            $user_search->query_where
        );
    }
}
add_action('pre_user_query','vm_pre_user_query');


//******************************************************************************
//prevent editor from deleting, editing, or creating an administrator
class VM_User_Caps {

	// Add our filters
	function __construct() {
		add_filter( 'editable_roles', array(&$this, 'editable_roles'));
		add_filter( 'map_meta_cap', array(&$this, 'map_meta_cap'),10,4);
	}

	// Remove 'Administrator' from the list of roles if the current user is not an admin
	function editable_roles( $roles ){
		if( isset( $roles['administrator'] ) && !current_user_can('administrator') ){
			unset( $roles['administrator']);
		}
		return $roles;
	}

	// If someone is trying to edit or delete an
	// admin and that user isn't an admin, don't allow it
	function map_meta_cap( $caps, $cap, $user_id, $args ){
		switch( $cap ){
			case 'edit_user':
			case 'remove_user':
			case 'promote_user':
			if( isset($args[0]) && $args[0] == $user_id )
			break;
			elseif( !isset($args[0]) )
			$caps[] = 'do_not_allow';
			$other = new WP_User( absint($args[0]) );
			if( $other->has_cap( 'administrator' ) ){
				if(!current_user_can('administrator')){
					$caps[] = 'do_not_allow';
				}
			}
			break;
			case 'delete_user':
			case 'delete_users':
			if( !isset($args[0]) )
			break;
			$other = new WP_User( absint($args[0]) );
			if( $other->has_cap( 'administrator' ) ){
				if(!current_user_can('administrator')){
					$caps[] = 'do_not_allow';
				}
			}
			break;
			default:
			break;
		}
		return $caps;
	}
}
$vm_user_caps = new VM_User_Caps();


//******************************************************************************
// Remove menus in WordPress dashboard

if ( current_user_can('administrator') ) {
	function my_remove_menu_pages() {
		remove_menu_page('edit-comments.php');        									// Reacties
		remove_submenu_page('options-general.php','options-discussion.php');            // Reacties
	}
	add_action( 'admin_menu', 'my_remove_menu_pages' );
}

if ( current_user_can('editor') ) {
	function my_remove_menu_pages() {
		remove_menu_page('tools.php');                								// Extra
		remove_menu_page('edit-comments.php');										// Reacties
		remove_submenu_page('options-general.php','options-discussion.php');        // Reacties

		global $submenu;
		unset($submenu['themes.php'][6]); 											// Customize

	}
	add_action( 'admin_menu', 'my_remove_menu_pages' );
}


//******************************************************************************
// Remove meta box
function remove_dashboard_meta() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'remove_dashboard_meta' );


//******************************************************************************
// Remove items from admin bar
add_action( 'admin_bar_menu', 'remove_items', 999 );

function remove_items( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'wp-admin-bar-comments' );
}


//******************************************************************************
// Remove items on WordPress dashboard
function remove_dashboard_widgets() {
    global $wp_meta_boxes;

    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

remove_action('welcome_panel', 'wp_welcome_panel');

?>
