<?php

// Enable featured image
add_theme_support( 'post-thumbnails' );

// excerpt
function tn_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'tn_custom_excerpt_length', 999 );

// svg
function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

// gutenberg
function legit_block_editor_styles() {
wp_enqueue_style( 'legit-editor-styles', get_theme_file_uri( '/style-editor.css' ), false, '2.3', 'all' );}
add_action( 'enqueue_block_editor_assets', 'legit_block_editor_styles' );

add_filter( 'gform_required_legend', '__return_empty_string' );
?>
