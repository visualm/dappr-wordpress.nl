<?php
//******************************************************************************
// Add ACF options page
if( function_exists('acf_add_options_page') ) {
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Thema Settings',
		'menu_title' 	=> 'Thema Settings',
		'menu_slug' 	=> 'thema-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
}

//******************************************************************************
// Registering multiple menus
register_nav_menus (
	array(
	'main-menu' => 'Main menu',
	'footer-menu' => 'Footer menu',
	)
);

//******************************************************************************
// Admin footer modification
function remove_footer_admin ()
{
    echo '<span id="footer-thankyou">Ontwikkeld door <strong><a href="http://www.dappr.nl/" target="_blank">dappr</a>.</strong></span>';
}
add_filter('admin_footer_text', 'remove_footer_admin');


//******************************************************************************
// Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

//******************************************************************************
// Registering multiple menus
register_nav_menus (
   array(
   'main-menu' => 'Main menu',
   'footer-menu' => 'Footer menu',
   )
);


//******************************************************************************
// Hide admin bar
add_filter('show_admin_bar', '__return_false');
?>
